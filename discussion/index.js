// npm init -y skips all the prompt and goes directly

/* 
  create an express variable that accepts a requiore("express") value
  store the "express()" function inside the "app" variable
  create a port variable that accepts 3000
  make your app able to read json as well as accept data from forms

  make your app listen/run to the port variable with a confirmation in the console "server is running at port"
*/

const express = require("express");

// mongoose - a package that allows creation of schemas to model the data structure and to have an access to a number of
const { default: mongoose } = require("mongoose");
const app = express();
const port = 3000;


mongoose.connect("mongodb+srv://evmalelang1:badminton05@wdc028-course-booking.jteye.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
useNewUrlParser: true,
useUnifiedTopology: true
})

// notification for connection : success/failure
let db=mongoose.connection;
// if an error existed in connecting to MongoDB
db.on("error", console.error.bind(console, "Connection Error"));

// if the connection is successful
db.once("open", ()=> console.log("We're connected to the database"))

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

// mongoose.schema - sets the structure of the document that is to be created; serves as the blueprint to the data/record
const taskSchema = new mongoose.Schema({
  name:String,
  status:{
    type: String,
    // default - sets the value once the field does not have any value entered in it
    default: "pending"
  }
})

// model - allows access to methods that will perform CRUD operations in the database; RULE - the first letter is always capital/uppercase for the variable of the model and must be singular form
/* 
  SYNTAX:
  const <variable> = mongoose.model(<variable>, <schemaName>)
*/

const Task = mongoose.model("Task", taskSchema)

// routes
/* 
business logic
1. check if the task is already existing, add the task in the database

2. the task will come from the request body

3. create a new task object with the needed properties

4. save to database
*/

app.post("/tasks", (req, res)=>{
  // checking for duplicate tasks
  // findOne is a mongoose method that acts similar to find in MongoDB; returns the first document it finds
  Task.findOne({name: req.body.name}, (error, result)=>{
    if(result !== null && result.name === req.body.name){
      return res.send("There is a duplicate task")
    } else {
      // if no task is found, create the object and save it to the db
      let newTask = new Task ({
        name:req.body.name
      })
      // saveErr - parameter that accepts errors, should there be any when saving the "newTask" object
      // savedTask - parameter that accepts the object should the saving of the "newTask" object is a success
      newTask.save((saveErr, savedTask)=>{
        // if there are errors. log in the console the error
        if(saveErr){
          console.error(saveErr);
        } else{
          // .status - returns a status (number code such as 201 for successful creation)
          return res.status(201).send("New Task Created")
        }
      })
    }
  })
})
/* 
1. retrieve all the documents using the find()
2. if an error is encountered, print the error (error handling)
3. if there are no errors, sund a success status back to the client and return and array of documents
*/
app.get("/tasks", (req, res) => {
  //find is similar to mongodb/robo3t find, setting up empty field
  Task.find({}, (error, result) => {
    if (error){
    return console.log(error)
  }else{
    return res.status(200).json({data:result}) //data field will be set and its value will be the result/response that we had for the request
  }
  
})
})


// create a function/route for a user registration
/*
1. Find if there is a duplicate user using the username field
    - if the user is existing, return "username already in use"
    - if the user is not existing, add it in the database
        - if the username and the password are filled, save
            - in saving, create a new User object (a User schema)
                - if there is an errror, log it in the console
                - if there is no error, send a status of 201 and "successfully registered"
        - if one of them is blank, send the response "BOTH username and password must be provided"
*/


// create a get request for getting all registered users
/*
    - retrieve all users registerd using find functionality
    - if there is an error, log it in the console
    - if there is no error, send a success status back to the client and return the array of users
*/

const userSchema = new mongoose.Schema({
  username:String,
  password:String
})

const User = mongoose.model("User", userSchema)


app.post("/registerUser", (req, res)=>{
  User.findOne({username: req.body.username}, (error, result)=>{
    if(result !== null && result.username === req.body.username){
      return res.send("username already in use")
    }else if(req.body.username==="" || req.body.password === ""){
      return res.send("BOTH username and password must be provided")
    }
else {

      let newUser = new User ({
        username:req.body.username,
        password:req.body.password
      })

      newUser.save((saveErr, savedUser)=>{

        if(saveErr){
          console.error(saveErr);
        } else{

          return res.status(201).send("Successfully registered")
        }
      })
    }
  })
})

app.get("/users", (req, res)=>{
	User.find( {}, (err, result)=>{
		if (err	) {
		return console.error(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}

	} )

})





app.listen(port, () => console.log(`Server is running at port ${port}`))
